// name    : utils.js
// author  : Simon Descarpentries, simon /\ acoeuro [] com
// licence : GPLv3

/* globals Uint8Array, Promise */
// https://stackoverflow.com/questions/3700326/decode-amp-back-to-in-javascript
export function HTML_decode_entities (s) {
	var elt = document.createElement('textarea')
	elt.innerHTML = s
	return elt.value
}
// https://stackoverflow.com/questions/18749591/encode-html-entities-in-javascript
// &laquo; -> « -> not an XML entity, so decode, recode in XML numerical entities
// <title> Xi & Macron </title> & -> &#038; (not re-encoded…)
// other XML entities are still missing
export function HTML_encode_UTF8 (s) {
	s = s.replace(/[\u00A0-\u9999&]/g, (i) => `&#${i.charCodeAt(0)};`)
	// s = s.replace(/[\u00A0-\u9999&]/g, (i) => {
	// 	console.log(`${i} &#${i.charCodeAt(0)};`); return `&#${i.charCodeAt(0)};` })
	// the following code works only with 1st occurence of fields
	/* s = s.replace(/>([^<]*?)>/g, ">$1&gt;")
	s = s.replace(/<([^>]*?)</g, "<$1&lt;")
	s = s.replace(/>([^.<]*?)'/g, ">$1&apos;")
	s = s.replace(/>([^.<]*?)"/g, ">$1&quot;") */
	return s
}
// https://stackoverflow.com/questions/11563554/how-do-i-detect-xml-parsing-errors-when-using-javascripts-domparser-in-a-cross
// parser and dom_err_NS could be cached on startup for efficiency
export const dom_parser = new DOMParser()
console.info("µtils : The next 'XML parse error: tag not closed' at line 1 col 1 is "
	+ "useful for µtils function 'isParserError'.")
const dom_err = dom_parser.parseFromString('<', 'text/xml'),
	dom_err_NS = dom_err.getElementsByTagName("parsererror")[0].namespaceURI
setTimeout(() => console.info("µtils : New errors are now not intended"), 0)
export function isParserError(dom) {
	/* if (dom_err_NS === 'http://www.w3.org/1999/xhtml') {
		// In PhantomJS the parserirror element doesn't seem to have a special namespace,
		// so we are just guessing here :(
		return dom.getElementsByTagName("parsererror").length > 0
	}*/
	return dom.getElementsByTagNameNS(dom_err_NS, 'parsererror').length > 0
}
export function get_wanted_locale() {
	return get_stored_locale() || navigator.language || navigator.userLanguage
}
export function isDarkMode() {return matchMedia('(prefers-color-scheme: dark)').matches}
export function get_wanted_theme() {
	return get_stored_theme() || (isDarkMode() && 'dark' || 'light')
}
export function set_theme() { if (get_wanted_theme() == 'light') set_light_mode() }
export function set_light_mode() {
	var html = document.getElementsByTagName('html')[0]
	html.style.cssText += "--background: var(--light-normal-background)"
	html.style.cssText += "--foreground: black"
	html.style.cssText += "--frame-background: var(--light-frame-background)"
	html.style.cssText += "--a-color: var(--mp-dark-turquoise)"
}
export const get_stored_locale = () => get_stored('locale', '')
export const get_stored_theme = () => get_stored('dark_background', '')
export const get_stored_live_search_reload = () => get_stored('live_search_reload', '')
export const get_stored_undup_results = () => get_stored('undup_results', '1')
export const get_stored_load_photos = () => get_stored('load_photos', '1')
export const get_stored_max_res_by_src = () => get_stored('max_res_by_src', 20)
export const get_stored_headline_loading = () => get_stored('headline_loading', '1')
export const get_stored_live_headline_reload = () => get_stored('live_headline_reload', '1')
export const get_stored_keep_host_perm = () => get_stored('keep_host_perm', '1')
export const get_stored_max_headline_loading =()=>get_stored('max_headline_loading', 30)
export const get_stored_headline_page_size = () => get_stored('headline_page_size', 5)
export function get_stored(key, default_value) {
	var local_data = localStorage.getItem(key)
	return local_data == null ? default_value : local_data
}
export function upload_file_to_user(file_name, str) {
	let a = document.createElement("a")
	a.href=`data:application/octet-stream;charset=UTF-8,${encodeURIComponent(str)}`
	a.download=file_name
	a.click()
}
export function ndt_human_readable() {
	return new Date().toISOString().replace("T", "_").replace(":", "h").split(":")[0] }
export function uuidv4() {
	return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	)
}
export function encode_XML(unsafe) {
	unsafe = removeXMLInvalidChars(unsafe)
	return unsafe.replace(new RegExp('[<>&\'"]', 'g'), (c) => {
		switch (c) {
		case "<": return "&lt;"
		case ">": return "&gt;"
		case "&": return "&amp;"
		case "'": return "&apos;"
		case "\"": return "&quot;"
		}
	})
}
/**
 * Removes invalid XML characters from a string
 * From https://gist.github.com/john-doherty/b9195065884cdbfd2017a4756e6409cc
 * @param {string} str - a string containing potentially invalid XML char. (non-UTF8, STX, EOX)
 * @param {boolean} removeDiscouragedChars - should it remove discouraged but valid XML char.
 * @return {string} a sanitized string stripped of invalid XML characters
 */
function removeXMLInvalidChars(str, removeDiscouragedChars=true) {
	// remove everything forbidden by XML 1.0 specifications, plus the unicode replacement
	// character U+FFFD
	var regex = new RegExp('((?:[\0-\x08\x0B\f\x0E-\x1F\uFFFD\uFFFE\uFFFF]|[\uD800-\uDBFF]' +
		'(?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]))', 'g')
	str = str.replace(regex, '')
	if (removeDiscouragedChars) {
		// remove everything discouraged by XML 1.0 specifications
		regex = new RegExp(
			'([\\x7F-\\x84]|[\\x86-\\x9F]|[\\uFDD0-\\uFDEF]|(?:\\uD83F[\\uDFFE\\uDFFF])|' +
		' (?:\\uD87F[\\uDFFE\\uDFFF])|(?:\\uD8BF[\\uDFFE\\uDFFF])|(?:\\uD8FF[\\uDFFE\\uDFFF])|'+
		'(?:\\uD93F[\\uDFFE\\uDFFF])|(?:\\uD97F[\\uDFFE\\uDFFF])|(?:\\uD9BF[\\uDFFE\\uDFFF])|'+
		'(?:\\uD9FF[\\uDFFE\\uDFFF])|(?:\\uDA3F[\\uDFFE\\uDFFF])|(?:\\uDA7F[\\uDFFE\\uDFFF])|'+
		'(?:\\uDABF[\\uDFFE\\uDFFF])|(?:\\uDAFF[\\uDFFE\\uDFFF])|(?:\\uDB3F[\\uDFFE\\uDFFF])|'+
		'(?:\\uDB7F[\\uDFFE\\uDFFF])|(?:\\uDBBF[\\uDFFE\\uDFFF])|(?:\\uDBFF[\\uDFFE\\uDFFF])|'+
		'(?:[\\0-\\t\\x0B\\f\\x0E-\\u2027\\u202A-\\uD7FF\\uE000-\\uFFFF]|[\\uD800-\\uDBFF]'+
		'[\\uDC00-\\uDFFF]|[\\uD800-\\uDBFF](?![\\uDC00-\\uDFFF])|(?:[^\\uD800-\\uDBFF]|^)'+
		'[\\uDC00-\\uDFFF]))', 'g')
		str = str.replace(regex, '')
	}
	return str
}
export function urlify(link, domain_part) { // return URL from any href (even relative links)
	if (link.startsWith("http")) {
		return link
	} else {
		if (link.startsWith("file")) {	// remove meta-press.es auto-added path
			return link.replace(document.URL.split("/").slice(0,-1).join("/"), domain_part)
		} else if (link.startsWith("//")) {
			return domain_part + "/" + link.split("/").slice(3).join("/")
		} else {
			return domain_part + (link.startsWith("/") ? "" : "/") + link
		}
	}
}
export function get_favicon_url(html_fragment, domain_part) { // favicon may be implicit
	var favicon = html_fragment.querySelector("link[rel~=\"icon\"]")
	return urlify(favicon && favicon.getAttribute("href") || "/favicon.ico", domain_part)
}
var intlNum = Intl.NumberFormat("fr", {minimumIntegerDigits: 4, useGrouping: 0})
export function timezoned_date (dt_str, tz="UTC") {
	var dt = dt_str ? new Date(dt_str) : new Date()
	if (isNaN(dt)) throw new Error(`${dt_str} is an invalid Date`)
	if (tz == "UTC" || tz == "GMT") return dt
	var dt_orig = new Date(dt.getTime() - dt.getTimezoneOffset()*60*1000)
	var dt_UTC = new Date(dt.getTime() + dt.getTimezoneOffset()*60*1000)
	var dt_repr = dt_UTC.toLocaleTimeString("fr", {timeZoneName: "short", timeZone: tz})
	var int_offset = parseInt(
		dt_repr.split("UTC")[1].replace("−", "-").replace(":", ".")*100)
	var tz_offset = intlNum.format(int_offset)
	var tz_repr = int_offset > 0 ? "+"+tz_offset : tz_offset
	return new Date(dt_orig.toISOString().replace(/\.\d{3}Z/, tz_repr))
}
export const bolden = (str, search) => str && str.replace(
	new RegExp(`(${preg_quote(search)})`, "gi"), "<b>$1</b>")
export function shorten(str, at) {
	let str2 = strip_html(str)
	let diff = str2.length - (at - 2)
	return diff > 0 ? `${str.slice(0, -diff)}…` : str
}
export function no_inline_style(str) { return str && str.replace(/style="[^"]+"/m, '') }
export function triw(str) { return str && str.replace(/^\s*|\s*$|(\s\s)+/gi, "") }
export function preg_quote(str) { // http://kevin.vanzonneveld.net, http://magnetiq.com
	// preg_quote("How many? $40"); -> 'How many\? \$40'
	// preg_quote("\\.+*?[^]$(){}=!<>|:"); -> '\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:'
	// str = String(str).replace(/([\\\.\+\*\?\[\^\]\$\(\)\{\}\=\!\<\>\|\:])/g, "\\$1")
	str = String(str).replace(/([\\.+*?[^\]$(){}=!<>|:])/g, "\\$1")
	return str.replace(/\s/g, "\\s")  // ensure we match converted &nbsp;
}
export function domain_part(url) { var [htt, , dom] = url.split("/")
	return `${htt}//${dom}` }
export function domain_name(url) { var [ , , dom] = url.split("/")
	var s_dom = dom.split(".")
	if (s_dom[0] == "www") s_dom = s_dom.slice(1, s_dom.length)
	return toTitleCase(s_dom.join("."))
}
export const toTitleCase = (str) => str.replace(/\w\S*/g, a =>
	`${a.charAt(0).toUpperCase()}${a.substr(1).toLowerCase()}`)
export const rnd = (n) => Math.ceil (Math.random () * Math.pow (10, n))
export const pick_between = (a, b) => Math.floor(Math.random() * b) + a
export const clean_c_type = (str) => str.split(";")[0].replace(/(rss|atom)\+/, "")
/* function format_search_url(search_url, token) {
	// 'http://a.aa/'.length == 12 -> fromIndex of indexOf
	switch (search_url.charAt(search_url.indexOf('{', 12) + 1)) {
		case '}': return search_url.replace('{}', token)
		case ' ': return search_url.replace('{}', token)
		case '+': return search_url.replace('{+}', token.replaceAll(' ', '+'))
		case '%': return search_url.replace('{%}', encodeURIComponent(token))
	}
}*/
export const sleep = (duration) => new Promise(resolve => setTimeout(resolve, duration))
// Evaluate an XPath expression aExpression against a given DOM node
// or Document object (aNode), returning the results as an array
// thanks wanderingstan at morethanwarm dot mail dot com for the
// initial work.
export function evaluateXPath(aNode, aExpr) {
	var xpe = new XPathEvaluator()
	var no_namespace = false
	var nsResolver
	if (no_namespace) {
		nsResolver = xpe.createNSResolver(aNode.ownerDocument == null ?
			aNode.documentElement : aNode.ownerDocument.documentElement)
	} else {
		nsResolver = (prefix) => {
			// Extended version of
			// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_using_XPath_in_JavaScript#Implementing_a_User_Defined_Namespace_Resolver
			var ns = {
				"xhtml" : "http://www.w3.org/1999/xhtml",
				"mathml": "http://www.w3.org/1998/Math/MathML",
				"content":"http://purl.org/rss/1.0/modules/content/",
				"wfw": "http://wellformedweb.org/CommentAPI/",
				"dc": "http://purl.org/dc/elements/1.1/",
				"atom": "http://www.w3.org/2005/Atom",
				"sy": "http://purl.org/rss/1.0/modules/syndication/",
				"slash": "http://purl.org/rss/1.0/modules/slash/",
				"media": "http://search.yahoo.com/mrss/",
			}
			return ns[prefix] || null
		}
	}
	var result = xpe.evaluate(aExpr, aNode, nsResolver, 0, null)
	var found = [], res
	while (res = result.iterateNext()) found.push(res)
	if (found.length == 1) found = found[0]
	return found
}
export function regextract (re, str, repl="$1") {
	return str.replace(new RegExp(`(?:.|\\n)*${re}(?:.|\\n)*`, "m"), repl) }
export const HH_MM_STR = "(\\d\\d:\\d\\d)"
export const HH_MM_RE = new RegExp(HH_MM_STR, 'i')
export const is_array = (v) => typeof(v) == 'object' && typeof(v.length) != 'undefined'
export function get_select_value(sel) { return sel.options[sel.selectedIndex].value }
export function deref_json_path(json_path, json_obj) {
	var val = json_obj
	if (json_path)
		for (let json_path_elt of json_path.split('.'))
			try {
				val = val[json_path_elt]
			} catch (exc) {
				console.error(exc)
			}
	return val
}
export function str_fmt(a, tokens) {
	for (var k in tokens)
		a = a.replace(new RegExp(`\\$${Number(k)+1}`, 'g'), tokens[k])
	return a
}
export function strip_html(s) { return s.replaceAll(/<[^>]*>?/gm, '') }
export function photo_loading_placeholder(s) {
	return s.replace(/<img([^>]*)src="([^"]*)"(:? srcset="[^"]*")?([^>]*)?>/g,
		'<img src="." $1 mp-data-img="$2" $3><a class="load_img" mp-data-img="$2">&#11015;</a>')
}
export function remove_photo_loading_handlers(s) { return s.replaceAll(/&#11015;/g, '') }
export function notifyUser(title, body) {
	// Let's check if the browser supports notifications
	if (!("Notification" in window)) console.warn("Browser don't support desktop notification")
	// Let's check whether notification permissions have already been granted
	else if (Notification.permission === "granted") createNotification(title, body)
	// If it's okay let's create a notification
	else if (Notification.permission !== "denied")
		// The Notification permission may only be requested from inside a short running
		// user-generated event handler.
		Notification.requestPermission().then(function (permission) {
			// If the user accepts, let's create a notification
			if (permission === "granted") createNotification(title, body)
		})
	// At last, if the user has denied notifications, and you
	// want to be respectful there is no need to bother them any more.
}
function createNotification(title, body) {
	new Notification(title, {
		body: body,
		icon: 'img/favicon-metapress-v2.png',
		image: 'img/logo-metapress_sq.svg',
		badge: 'img/favicon-metapress-v2.png',
		actions: [{}]
	})
}
